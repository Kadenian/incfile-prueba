<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Mail;

class enviaremails extends Command
{
    protected $signature = 'enviar:email {--to=}{--from=}{--titulo=}{--name=}';

    protected $description = 'Sending emails';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {

        $to = $this->option('to');
        $from = $this->option('from');
        $titulo = $this->option('titulo');
        $name = $this->option('name');
        
        $data = array(
            'name' => "$name"
       );

        Mail::send("emailtest", $data, function ($message) use ($to, $from, $titulo) {
            

            $message->from($from);

            $message->to($to)->subject($titulo);

        });
        $this->info('The emails are send successfully!');
    }
}
